# gce-demo

Google compute engine建立VM，並進行簡單的程式練習

### linux 從Domain查詢IP

首先，要知道為什麼我們可以從一個網址的Domain name就知道他的IP位置，就得先了解DNS的運作方式，以及為什麼我們需要DNS。

src: [主機名稱控制者：DNS控制器](http://linux.vbird.org/linux_server/0350dns.php)
[DNS Hijacking](https://www.gohacking.com/dns-hijacking/)


#### 什麼是DNS?

DNS，也就是網域名稱系統，將一般人可閱讀的網域名稱(eg. www.google.com) 轉換為可讓機器讀取的IP位址 (eg.172.217.160.110) ，當我們利用Internet傳送資料時，才知道要將資料封包傳送到哪。


#### 應用練習

- `dig` : 未加參數則顯示出此domain所有詳細資料
  
    `+trace` 可以把DNS查詢路徑列出來
    `+short` 只列出ip

- 應用：寫一支程式用 `dig` 把一份文件裡多個domain各自的ip列出來
- 步驟：
    1. 新增一份domain.txt，裡面列出多個domain如圖：
```
tw.yahoo.com
google.com.tw
ntust.edu.tw
www.ntu.edu.tw
nctu.edu.tw
www.edu.tw
```
2. 新增一個checkip.py去抓這份文件裡每個domain，並用 `dig` 抓他的ip

```python
import os
import sys
fileName = input("Choose a file: ")
myFile = open(fileName)
for line in myFile.readlines():
  domain = line.replace("\n", " ")
  print(domain)
  os.system("dig {} +short".format(domain))
  print('\n')
```
